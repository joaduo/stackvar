import stackvar as sv


def test():
    try:
        intermediary_func1()
        raise AssertionError('Should raise TypeError')
    except TypeError as exc:
        assert 'receiving_function() missing 2 required positional stack variables:' in exc.args[0]
    try:
        receiving_function()
        raise AssertionError('Should raise TypeError')
    except TypeError as exc:
        assert 'receiving_function() missing 2 required positional stack variables:' in exc.args[0]
    try:
        receiving_function(foo=1)
        raise AssertionError('Should raise TypeError')
    except TypeError as exc:
        assert 'receiving_function() missing 1 required positional stack variable:' in exc.args[0]

 
def intermediary_func1():
    intermediary_func2()
 
 
def intermediary_func2():
    receiving_function()


@sv.receive()
def receiving_function(baz, foo: sv.Variable, bar: sv.Variable):
    print(foo, bar, baz)


if __name__ == '__main__':
    test()

