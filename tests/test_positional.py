import stackvar as sv


def test():
    # Sending within a context
    with sv.send(receiving_function, foo_value='sent'):
        assert intermediary_func1() == 'sent'
    # Using the default value
    for func in (receiving_function, intermediary_func1):
        try:
            assert func() == 'fake_default'
            raise AssertionError('Should raise TypeError')
        except TypeError as exc:
            assert 'receiving_function() missing 1 required positional stack variable: ' in exc.args[0]
    # Overriding before dispatching
    assert intermediary_func1(override=True) == 'overriden'
    # Calling directly passing the value
    assert receiving_function(foo_value='passed') == 'passed'

 
def intermediary_func1(override=False):
    if override:
        with sv.send(receiving_function, foo_value='overriden'):
            return intermediary_func2()
    return intermediary_func2()
 
 
def intermediary_func2():
    return receiving_function()


@sv.receive()
def receiving_function(foo_value: sv.Variable):
    return foo_value


if __name__ == '__main__':
    test()
