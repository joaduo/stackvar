import stackvar as sv
import uuid

sv_namespace = sv.Namespace(uuid.uuid4())

def test():
    # Sending within a context
    with sv.send(sv_namespace, foo_value='sent'):
        assert intermediary_func1() == 'sent'
    # Using the default value
    assert receiving_function() == 'default'
    assert intermediary_func1() == 'default'
    # Overriding before dispatching
    assert intermediary_func1(override=True) == 'overriden'
    # Calling directly passing the value
    assert receiving_function(foo_value='passed') == 'passed'
    assert receiving_function('passed') == 'passed'
    TestClass().test()

 
def intermediary_func1(override=False):
    if override:
        with sv.send(sv_namespace, foo_value='overriden'):
            return intermediary_func2()
    return intermediary_func2()
 
 
def intermediary_func2():
    return receiving_function()


@sv.receive(sv_namespace)
def receiving_function(foo_value: sv.Variable = 'default'):
    return foo_value


class TestClass:
    def test(self):
        # Sending within a context
        with sv.send(sv_namespace, foo_value='sent'):
            assert self.intermediary_func1() == 'sent'
        # Using the default value
        assert self.receiving_function() == 'default'
        assert self.intermediary_func1() == 'default'
        # Overriding before dispatching
        assert self.intermediary_func1(override=True) == 'overriden'
        # Calling directly passing the value
        assert self.receiving_function(foo_value='passed') == 'passed'
        assert self.receiving_function('passed') == 'passed'

    def intermediary_func1(self, override=False):
        if override:
            with sv.send(sv_namespace, foo_value='overriden'):
                return self.intermediary_func2()
        return self.intermediary_func2()

    def intermediary_func2(self):
        return self.receiving_function()

    @sv.receive(sv_namespace)
    def receiving_function(self, foo_value: sv.Variable = 'default'):
        if hasattr(sv_namespace, 'foo_value'):
            assert foo_value == sv_namespace.foo_value 
        return foo_value


if __name__ == '__main__':
    test()

