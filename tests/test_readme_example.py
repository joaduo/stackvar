import stackvar

def test():
    """
    Dispatching stack variables to function parameters
    """
    # sending within a context
    with stackvar.send(receiving_function, foo_value='sent'):
        intermediary_func1()
    # passing parameter
    receiving_function(foo_value='passed')
    # using the default value
    intermediary_func1()
    receiving_function()


def intermediary_func1():
    intermediary_func2()
  
def intermediary_func2():
    receiving_function()

@stackvar.receive()
def receiving_function(foo_value: stackvar.Variable = 'default'):
    print(f'foo_value={foo_value}')

if __name__ == '__main__':
    test()