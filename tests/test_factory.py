import stackvar as sv
from collections import defaultdict


def test():
    assert intermediary_func1() == {'count': 1}
    foo = defaultdict(int)
    foo['count'] = 2
    with sv.send(receiving_function, foo=foo):
        assert intermediary_func1() == {'count': 3}
    foo = defaultdict(int)
    foo['count'] = 5
    assert receiving_function(foo) == {'count': 6}
    
 
def intermediary_func1():
    return intermediary_func2()
 
 
def intermediary_func2():
    return receiving_function()


@sv.receive()
def receiving_function(foo: sv.Factory= lambda: defaultdict(int)):
    foo['count'] += 1
    return foo


if __name__ == '__main__':
    test()

