import stackvar as sv


def test():
    @sv.receive()
    def no_stack_vars(a,b):
        ...
    assert not hasattr(no_stack_vars, '__wrapped__')
    @sv.receive()
    def stack_vars(foo_value: sv.Variable = 'default'):
        ...
    assert hasattr(stack_vars, '__wrapped__')


if __name__ == '__main__':
    test()
