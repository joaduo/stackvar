import stackvar as sv
import uuid


def test():
    TestClass().test()


class TestClass:
    stackvar_ns = uuid.uuid4()

    def test(self):
        # Sending within a context
        with sv.send(self.stackvar_ns, foo_value='sent'):
            assert self.intermediary_func1() == 'sent'
        # Using the default value
        assert self.receiving_function() == 'default'
        assert self.intermediary_func1() == 'default'
        # Overriding before dispatching
        assert self.intermediary_func1(override=True) == 'overriden'
        # Calling directly passing the value
        assert self.receiving_function(foo_value='passed') == 'passed'
        assert self.receiving_function('passed') == 'passed'

    def intermediary_func1(self, override=False):
        if override:
            with sv.send(self.stackvar_ns, foo_value='overriden'):
                return self.intermediary_func2()
        return self.intermediary_func2()

    def intermediary_func2(self):
        return self.receiving_function()

    @sv.receive(stackvar_ns)
    def receiving_function(self, foo_value: sv.Variable = 'default'):
        return foo_value


if __name__ == '__main__':
    test()
