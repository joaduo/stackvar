import stackvar as sv

def test():
    # Sending within a context
    with sv.send('mynamespace', foo_value='sent'):
        assert intermediary_func1() == 'sent'
    # Using the default value
    assert receiving_function() == 'default'
    assert intermediary_func1() == 'default'
    # Overriding before dispatching
    assert intermediary_func1(override=True) == 'overriden'
    # Calling directly passing the value
    assert receiving_function(foo_value='passed') == 'passed'
    assert receiving_function('passed') == 'passed'

 
def intermediary_func1(override=False):
    if override:
        with sv.send('mynamespace', foo_value='overriden'):
            return intermediary_func2()
    return intermediary_func2()
 
 
def intermediary_func2():
    return receiving_function()


@sv.receive('mynamespace')
def receiving_function(foo_value: sv.Variable = 'default'):
    return foo_value


if __name__ == '__main__':
    test()

