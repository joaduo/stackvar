import stackvar as sv
# import stackvar


def test():
    # Sending withing a context
    with sv.send(receiving_function, foo_value='sent'):
        assert intermediary_func1() == 'sent'
    # Overriding before dispatching
    assert intermediary_func1(override=True) == 'overriden'
    with sv.send(receiving_function2, foo=1, bar=2):
        assert receiving_function2() == (1, 2)
    with sv.send(receiving_function2, foo=1):
        assert receiving_function2() == (1, None)
    with sv.send(receiving_function3, foo=1, bar=2):
        assert receiving_function3() == (1, 2)
    with sv.send(receiving_function3, foo=1):
        try:
            receiving_function3()
            raise AssertionError('Should raise AttributeError')
        except AttributeError:
            pass


def receiving_function2():
    foo, bar = sv.get(receiving_function2, foo=None, bar=None)
    return foo, bar


def receiving_function3():
    ns = sv.Namespace(receiving_function3)
    foo, bar = ns.foo, ns.bar
    return foo, bar

 
def intermediary_func1(override=False):
    if override:
        with sv.send(receiving_function, foo_value='overriden'):
            return intermediary_func2()
    return intermediary_func2()
 
 
def intermediary_func2():
    return receiving_function()


def receiving_function():
    foo_value = sv.get(receiving_function, foo_value='default')
    assert foo_value == sv.get(receiving_function, 'foo_value')
    assert foo_value == sv.Namespace(receiving_function).foo_value
    return foo_value


if __name__ == '__main__':
    test()
