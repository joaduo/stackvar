import stackvar as sv


def test():
    # Sending within a context
    with sv.send(receiving_function1, foo_value='sent1'):
        with sv.send(receiving_function2, foo_value='sent2'):
            assert intermediary_func1() == ('sent1', 'sent2')
    # Sending to multiple namespaces
    with sv.send(receiving_function1,
                 receiving_function2,
                 foo_value='sent_to_both'):
            assert intermediary_func1() == ('sent_to_both', 'sent_to_both')
    # Using the default value
    assert intermediary_func1() == ('default1', 'default2')

 
def intermediary_func1():
    return receiving_function1(), receiving_function2()


@sv.receive()
def receiving_function1(foo_value: sv.Variable = 'default1'):
    return foo_value


@sv.receive() 
def receiving_function2(foo_value: sv.Variable = 'default2'):
    return foo_value


if __name__ == '__main__':
    test()
