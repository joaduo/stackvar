import stackvar as sv
import uuid


def test():
    # Sending within a context
    myinstance = MyClass()
    with sv.send(MyClass.receiving_function, foo_value='sent'):
        assert myinstance.intermediary_func1() == 'sent'
    # Using the default value
    assert myinstance.receiving_function() == 'default'
    assert myinstance.intermediary_func1() == 'default'
    # Overriding before dispatching
    assert myinstance.intermediary_func1(override=True) == 'overriden'
    # Calling directly passing the value
    assert myinstance.receiving_function(foo_value='passed') == 'passed'
    assert myinstance.receiving_function('passed') == 'passed'
    assert MyClass1().intermediary_func1() == ('foo_sent', 'bar_sent')


class MyClass:
    def intermediary_func1(self, override=False):
        if override:
            with sv.send(MyClass.receiving_function, foo_value='overriden'):
                return self.intermediary_func2()
        return self.intermediary_func2()

    def intermediary_func2(self):
        return self.receiving_function()

    @sv.receive()
    def receiving_function(self, foo_value: sv.Variable = 'default'):
        return foo_value



class MyClass1:
    CLASS_NS = uuid.uuid4()
    def intermediary_func1(self):
        with sv.send(MyClass1.CLASS_NS,
                     foo='foo_sent',
                     bar='bar_sent'):
            return self.receiving_function()

    @sv.receive(CLASS_NS)
    def receiving_function(self, foo: sv.Variable = 'default'):
        class InlineClass:
            @sv.receive(MyClass1.CLASS_NS)
            def receiving2(self, bar: sv.Variable):
                return bar
        return foo, InlineClass().receiving2()


if __name__ == '__main__':
    test()

